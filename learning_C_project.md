### INTRODUZIONE AL PROGETTO LEARNING-\<Programming Language\>

Il progetto Learning-\<Programming Language\> mira a realizzare un manuale 
code oriented su alcuni dei linguaggi di programmazione maggiormente 
utilizzati sui sistemi operativi UNIX con kernel Linux.

Per code oriented si intende anzitutto immediata operativita', nel senso che
non ci saranno pagine da sfogliare ma solo e soprattutto righe di codice da
leggere ed interpretare, commenti mirati inoltre saranno inseriti all'interno
del codice stesso laddove ce ne fosse bisogno.

I libri talvolta si perdono in inutili dettagli, tralasciando troppo spesso
il codice, L-\<PL\> invece si pone l'obiettivo di fare il lavoro opposto, e
rendere al codice la giusta collocazione che merita, ossia di primo attore
indiscusso.

Oltre alle regole fondamentali di ciascun linguaggio di programmazione, 
indipendentemente dal paradigma utilizzato, si prestera' particolare 
attenzione alle tecniche per scrivere codice di qualita' e sicuro.

Al momento della stesura di questo documento sono stati scelti due paradigmi
di programmazione e due linguaggi specifici; il paradigma Object Oriented 
con il linguaggio C++ e il paradigma procedurale con il linguaggio C.

Infine un ulteriore repository sara' dedicato alla programmazione di sistema
su sistemi operativi unix-like con kernel Linux e un altro ancora interamente 
incentrato sul magnifico linguaggio Perl, che nei sistemi operativi unix-like 
trova il suo habitat naturale

### LEARNING-C - Paradigma procedurale (imperativo), Standard ISO (c99, c11)

Il presente repository riguarda il paradigma procedurale, presentato mediante
il linguaggio di programmazione C-ANSI (c99, c11);  è stato contrassegnato
come L-C.

### Stato dell'opera

* Learning C
* Learning C++
* Learning Perl
