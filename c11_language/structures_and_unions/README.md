### List of arguments covered

* Structures;
* How to copy a structure;
* Size of structure;
* New type with typedef;
* Pointer to nested structures;
* Unions;
* Bit fields;
* vector of structures.

### Bibliograhy

[The Lost Art of C Structure Packing](https://www.catb.org/esr/structure-packing/) 
by *Eric S.Rymond*
