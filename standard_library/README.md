## C Standard Library (ISO/IEC 9899:2011, aka c11)

* <assert.h>        - Diagnostics
* <complex.h>       - Complex arithmetic (since c99)
* <ctype.h>         - Character handling
* <errno.h>         - Errors
* <fenv.h>          - Floating-point environment (since c99)
* <float.h>         - Characteristics of floating types
* <inttypes.h>      - Format conversions of integer types (since c99)
* <iso646.h>        - Alternative spellings (since c95)
* <limits.h>        - Sizes of integer types
* <locale.h>        - Localization
* <math.h>          - Mathematics
* <setjmp.h>        - Nonlocal jumps
* <signal.h>        - Signal handling
* <stdalign.h>      - Alignment (since c11)
* <stdarg.h>        - Variable arguments
* <stdatomic.h>     - Atomics (since c11)
* <stdbool.h>       - Boolean type and values (since c99)
* <stddef.h>        - Commom definitions
* <stdint.h>        - Integer types (since c99)
* <stdio.h>         - Input/output
* <stdlib.h>        - General utilities
* <stdnoreturn.h>   - `_Noreturn` (since c11)
* <string.h>        - String handling
* <tgmath.h>        - Type-generic math (since c99)
* <threads.h>       - Threads (since c11)
* <time.h>          - Date and time
* <uchar.h>         - Unicode utilities (c11)
* <wchar.h>         - Extended multibyte and wide character utilities (c95)
* <wctype.h>        - Wide character classification and mapping utilities (c95)
